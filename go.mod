module bitbucket.org/kpetku/lunta

go 1.13

require (
	fyne.io/fyne v1.1.2
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw v0.0.0-20190409004039-e6da0acd62b1 // indirect
	github.com/mitchellh/go-wordwrap v1.0.0
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	golang.org/x/net v0.0.0-20191028085509-fe3aa8a45271 // indirect
	golang.org/x/text v0.3.2 // indirect
)
