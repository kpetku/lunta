lunta
=====
A heavily opinionated, experimental, project gemini client.  Probably not ready for general use.

# Known issues
* Horizontal scrolling is not yet implemented upstream: long lines will not word break.
* Forward/back buttons are not fully implemented.
* Android support is not yet implemented upstream.
* Certificate support is missing.

# TODO
* mimetype support
* Lots...

# License
```
This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details. 
```
