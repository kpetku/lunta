package client

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net"
	"net/textproto"
	"strconv"
	"strings"
	"time"
)

const (
	Input                     = 1
	Success                   = 2
	Redirect                  = 3
	TemporaryFailure          = 4
	PermanentFailure          = 5
	ClientCertificateRequired = 6
)

var (
	ErrReadingHandshake    = errors.New("reading handshake")
	ErrInvalidParameters   = errors.New("invalid parameters")
	ErrFetchingDestination = errors.New("fetching destination")
	ErrTemporaryError      = errors.New("temporary error")
	ErrInternalServerError = errors.New("internal server error")
	ErrMalformedHeader     = errors.New("malformed header")
	ErrFetchFailed         = errors.New("fetch failed")
	ErrUnknown             = errors.New("unknown error")
)

type Response struct {
	Status int
	Meta   string
	Result string
	R      *io.Reader
	Err    error
}

// TODO: return an io.Reader instead of passing string
func Get(host, resource string) *Response {
	fr := new(Response)
	var result strings.Builder
	resource = strings.TrimPrefix(resource, "gemini://")
	resource = strings.TrimPrefix(resource, host)
	resource = strings.TrimSuffix(resource, ":1965")

	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	d := net.Dialer{
		Timeout: time.Second * 10,
	}
	conn, err := tls.DialWithDialer(&d, "tcp", host+":1965", conf)

	if err != nil {
		fr.Err = err
		return fr
	}
	defer conn.Close()

	_, err = conn.Write([]byte("gemini://" + host + resource + "\r\n"))
	if err != nil {
		conn.Close()
		fr.Err = err
		return fr
	}
	c := textproto.NewConn(conn)
	response, err := c.ReadLine()
	if err != nil {
		conn.Close()
		fr.Err = fmt.Errorf("Error %w: %w", ErrReadingHandshake, err)
		return fr
	}
	resp := strings.Fields(response)
	if len(resp[0]) != 2 || len(resp[1]) > 1024 {
		conn.Close()
		fr.Err = ErrMalformedHeader
		return fr
	}
	meta := strings.Join(resp[1:], " ")
	fr.Meta = meta
	statusCode, err := strconv.Atoi(response[0:1])
	if err != nil {
		conn.Close()
		fr.Err = ErrMalformedHeader
		return fr
	}
	fr.Status = statusCode
	switch statusCode {
	case Input:
		return fr
	case Success:
		break
	case Redirect:
		return fr
	case TemporaryFailure:
		fr.Err = ErrTemporaryError
		return fr
	case PermanentFailure:
		fr.Err = ErrInternalServerError
		return fr
	default:
		fr.Err = ErrUnknown
		return fr
	}
	for {
		l, err := c.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			conn.Close()
			fr.Err = ErrMalformedHeader
			return fr
		}
		result.WriteString(l + "\n")
	}
	conn.Close()
	fr.Result = result.String()
	return fr
}
