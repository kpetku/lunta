package main

import (
	"log"
	"os"

	"bitbucket.org/kpetku/lunta/gui"
)

func main() {
	os.Setenv("FYNE_FONT_MONOSPACE", "")
	gui.New()
	log.SetFlags(log.Lshortfile)
}
