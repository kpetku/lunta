package gui

import (
	"fyne.io/fyne"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
)

type header struct {
	textArea *widget.Entry
	back     []string
	next     []string
}

func (mw *mainWindow) newAddressBar() *fyne.Container {
	confirmButton := widget.NewToolbar(
		widget.NewToolbarAction(theme.ConfirmIcon(), func() {
			if mw.header.textArea.Text == "" {
				mw.GrabLink(mw.header.textArea.PlaceHolder, false)
				return
			}
			mw.GrabLink(mw.header.textArea.Text, false)
		}),
	)
	topButtons := widget.NewToolbar(
		widget.NewToolbarAction(theme.NavigateBackIcon(), func() {
			if mw.currentURI != "" {
				mw.header.next = append(mw.header.next, mw.currentURI)
				mw.GrabLink(mw.header.back[len(mw.header.back)-1], true)
				mw.header.back = mw.header.back[:len(mw.header.back)-1]
			}
		}),
		widget.NewToolbarAction(theme.NavigateNextIcon(), func() {
			if mw.currentURI != "" {
				mw.header.back = append(mw.header.back, mw.currentURI)
				if len(mw.header.next) > 1 {
					mw.GrabLink(mw.header.next[len(mw.header.next)-1], true)
					mw.header.next = mw.header.next[:len(mw.header.next)-1]
				}
			}
		}),
		widget.NewToolbarAction(theme.ViewRefreshIcon(), func() {
			if mw.header.textArea.Text == "" {
				mw.GrabLink(mw.header.textArea.PlaceHolder, true)
				return
			}
			mw.GrabLink(mw.currentURI, true)
		}),
		widget.NewToolbarAction(theme.HomeIcon(), func() {
			mw.SetText(sampleContent)
			mw.updateAddrBox("")
		}),
		widget.NewToolbarSeparator(),
	)
	return fyne.NewContainerWithLayout(layout.NewGridLayout(3), topButtons, mw.header.textArea, confirmButton)
}

func (mw *mainWindow) updateAddrBox(s string) {
	mw.header.textArea.SetText(s)
	mw.currentURI = s
	return
}
