package gui

import (
	"image/color"
	"log"
	"net/url"
	"strings"
	"time"

	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
	"github.com/mitchellh/go-wordwrap"
)

const gemini = "gemini://"

var sampleContent = `
Welcome to lunta, an experimental graphical Gemini client.
Click a link to get started or enter your Gemini site in the bar above.

=> gemini://zaibatsu.circumlunar.space/		Project Gemini
=> gemini://carcosa.net/			Carcosa.Net
=> gemini://heavysquare.com/			HeavySquare
=> gemini://mozz.us/				Mozz
=> gemini://dgold.eu/				DGold
=> gemini://typed-hole.org/			TypedHole
=> gemini://gopher.black/			Gemini Black
=> gemini://tilde.black/			Tilde Black
=> gemini://consensus.circumlunar.space		Red Consensus
=> gemini://tilde.pink/				Tilde Pink
`

// New launches a new GUI
func New() {
	a := app.New()
	a.Settings().SetTheme(theme.DarkTheme())

	mw := initMainWindow(a)

	mw.wrapColumns = 80
	addressBar := mw.newAddressBar()
	bottomBar := fyne.NewContainerWithLayout(layout.NewGridLayout(2), mw.footer.bottomBarURL, mw.footer.progressBar)
	primaryLayout := fyne.NewContainerWithLayout(layout.NewBorderLayout(addressBar, bottomBar, makeCell(), makeCell()), addressBar, mw.scrollBar, bottomBar)

	mw.window.SetContent(primaryLayout)
	mw.window.Resize(fyne.NewSize(480, 320))
	mw.SetText(sampleContent)

	mw.window.SetMainMenu(mw.mainMenu())
	mw.window.ShowAndRun()
}

func (mw *mainWindow) GrabLink(input string, fromToolbar bool) {
	mw.footer.showProgressBar()
	mw.footer.bottomBarURL.Hide()

	defer func() {
		time.Sleep(time.Second * 1)
		mw.footer.hideProgressBar()
		mw.footer.bottomBarURL.Show()
	}()
	u := mw.link2url(input)

	if u.Scheme == "gemini" {
		mw.GrabHelper(u, fromToolbar)
		return
	}
	dg := dialog.NewConfirm("Unknown link detected", wordwrap.WrapString("Do you want to launch a web browser for: "+input, mw.wrapColumns), func(b bool) {
		if b {
			u, err := url.Parse(input)
			if err != nil {
				log.Printf("Error parsing URL: %s: %s", input, err)
				return
			}
			mw.app.OpenURL(u)
		} else {
			return
		}
	}, mw.window)
	dg.Show()
}

func (mw *mainWindow) GrabHelper(u *url.URL, fromToolbar bool) {
	var copyText string
	if !u.IsAbs() {
		base, err := url.Parse(mw.currentURI)
		if err != nil {
			log.Fatal(err)
		}

		u.Path = base.ResolveReference(u).String()
		c := mw.fetch(u.Host, u.Path)
		if c.Err != nil {
			dialog.ShowError(c.Err, mw.window)
		}
		copyText = c.Result
	} else {
		c := mw.fetch(u.Host, u.Path)
		copyText = c.Result
		if c.Err != nil {
			dialog.ShowError(c.Err, mw.window)
		}
	}

	mw.SetText(copyText)
	mw.updateAddrBox(u.String())

	if !fromToolbar {
		mw.header.next = nil
		mw.header.back = append(mw.header.back, mw.currentURI)
	}
}

type mainWindow struct {
	app    fyne.App
	window fyne.Window

	header    *header
	body      *widget.Box
	footer    *footer
	scrollBar *widget.ScrollContainer

	currentURI  string
	wrapColumns uint
}

func (mw *mainWindow) SetText(s string) {
	var flush bool
	mw.footer.bottomBarURL.SetText("")
	mw.body.Children = []fyne.CanvasObject{}

	temp := strings.Split(s, "\n")
	var something strings.Builder
	for i, line := range temp {
		if strings.HasPrefix(line, "=>") {
			line = strings.TrimPrefix(line, "=>")
			e := widget.NewLabelWithStyle("", fyne.TextAlignLeading, fyne.TextStyle{Bold: false, Italic: false, Monospace: true})
			if something.Len() > 0 && !flush {
				e.SetText(something.String())

				mw.body.Append(e)
				something.Reset()
				flush = true
			}

			// TODO: Handle links properly
			var before, after string
			split := strings.Fields(line)
			if len(split) > 0 {
				before = wordwrap.WrapString(split[0], mw.wrapColumns)
			}
			if len(split) == 2 {
				after = wordwrap.WrapString(split[1], mw.wrapColumns)
			}
			// the real link is split[1]
			if len(split) > 2 {
				mw.body.Append(mw.NewLink(wordwrap.WrapString(strings.Join(split[1:], " "), mw.wrapColumns), before))
			} else if len(split) == 2 {
				mw.body.Append(mw.NewLink(after, before))
			} else {
				if len(split) > 0 {
					mw.body.Append(mw.NewLink(before, before))
				}
			}
		} else {
			something.WriteString(wordwrap.WrapString(line, mw.wrapColumns))
			something.WriteString("\n")

		}
		if something.Len() > 0 && flush || i == len(temp)-1 {
			e := widget.NewLabelWithStyle("", fyne.TextAlignLeading, fyne.TextStyle{Bold: false, Italic: false, Monospace: true})
			e.SetText(something.String())

			mw.body.Append(e)
			something.Reset()
			flush = false
		}
	}
	mw.footer.hideProgressBar()
	// TODO: Remove this when upstream gets more stable
	mw.scrollBar.CreateRenderer().Refresh()
}

func initMainWindow(app fyne.App) *mainWindow {
	mw := new(mainWindow)
	mw.app = app

	mw.body = widget.NewVBox()
	mw.scrollBar = widget.NewScrollContainer(mw.body)

	mw.footer = new(footer)
	mw.footer.bottomBarURL = widget.NewLabel("")

	mw.header = new(header)
	mw.header.back = make([]string, 100)
	mw.header.next = make([]string, 100)

	mw.footer.progressBar = widget.NewProgressBarInfinite()
	mw.footer.hideProgressBar()

	mw.header.textArea = widget.NewEntry()
	mw.header.textArea.SetPlaceHolder("gemini://gemini.conman.org")

	mw.window = app.NewWindow("lunta " + version)

	return mw
}

func makeCell() fyne.CanvasObject {
	rect := canvas.NewRectangle(&color.RGBA{128, 128, 128, 255})
	rect.SetMinSize(fyne.NewSize(600, 800))
	return rect
}

func (mw *mainWindow) mainMenu() *fyne.MainMenu {
	return fyne.NewMainMenu(fyne.NewMenu("File",
		fyne.NewMenuItem("New", func() {}),
	),
		fyne.NewMenu("Theme",
			fyne.NewMenuItem("Light", func() { mw.app.Settings().SetTheme(theme.LightTheme()) }),
			fyne.NewMenuItem("Dark", func() { mw.app.Settings().SetTheme(theme.DarkTheme()) }),
		),
		fyne.NewMenu("Columns",
			fyne.NewMenuItem("40", func() { mw.wrapColumns = uint(40) }),
			fyne.NewMenuItem("72", func() { mw.wrapColumns = uint(72) }),
			fyne.NewMenuItem("80", func() { mw.wrapColumns = uint(80) }),
		))
}

func init() {
	hyperlinkIcon.SetMinSize(fyne.NewSize(15, 15))
}
