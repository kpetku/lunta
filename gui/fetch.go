package gui

import (
	"net/url"
	"strings"

	client "bitbucket.org/kpetku/lunta/gemini"
	"fyne.io/fyne"
	"fyne.io/fyne/dialog"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"github.com/mitchellh/go-wordwrap"
)

// TODO: return an io.Reader instead of passing string
func (mw *mainWindow) fetch(host, resource string) *client.Response {
	fr := client.Get(host, resource)
	switch fr.Status {
	case client.Input:
		v := fyne.NewContainerWithLayout(layout.NewGridLayout(1))
		e := widget.NewEntry()

		v.AddObject(widget.NewLabel(wordwrap.WrapString(fr.Meta, mw.wrapColumns)))
		v.AddObject(e)

		dialog.ShowCustomConfirm("Input required", "Send", "Cancel", v, func(b bool) {
			if b {
				// TODO: parse uri's and remove the query string the right way
				if strings.Contains(resource, "?") {
					resource = strings.Split(resource, "?")[0]
				}
				u := new(url.URL)
				u.Scheme = "gemini"
				u.Host = host
				u.Path = resource + "?" + e.Text
				mw.GrabHelper(u, false)
			}
		}, mw.window)
		return fr
	case client.Success:
		break
	case client.Redirect:
		input := fr.Meta
		u, err := url.Parse(input)
		if err != nil {
			dialog.ShowError(fr.Err, mw.window)
			return fr
		}
		// Follow the link if it's relative or on the same host
		if u.Host == host || !u.IsAbs() {
			go mw.GrabLink(u.String(), false)
		} else {
			// TODO: handle redirect loops...
			dg := dialog.NewConfirm("Resource has moved", wordwrap.WrapString("Do you want to follow this redirect: "+input, mw.wrapColumns), func(b bool) {
				if b {
					go mw.GrabLink(u.String(), true)
				}
			}, mw.window)
			dg.Show()
		}
		return fr
	case client.TemporaryFailure, client.PermanentFailure:
		break
	default:
	}
	return fr
}
