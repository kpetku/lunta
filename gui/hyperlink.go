package gui

import (
	"log"
	"net/url"
	"strings"

	"fyne.io/fyne"
	"fyne.io/fyne/canvas"
	"fyne.io/fyne/driver/desktop"
	"fyne.io/fyne/theme"
	"fyne.io/fyne/widget"
)

type hyperlink struct {
	*widget.Box
	url     string
	hovered bool
	mw      *mainWindow
}

var hyperlinkIcon = &canvas.Image{
	Resource: theme.FolderIcon(),
	FillMode: canvas.ImageFillContain,
}

func (l *hyperlink) MouseIn(*desktop.MouseEvent) {
	if len(l.url) > int(l.mw.wrapColumns)-3 {
		tmp := l.url[:l.mw.wrapColumns-3] + "..."
		l.mw.footer.bottomBarURL.SetText(l.mw.link2url(tmp).String())
	} else {
		l.mw.footer.bottomBarURL.SetText(l.mw.link2url(l.url).String())
	}
	l.hovered = true

}
func (l *hyperlink) MouseOut() {
	l.mw.footer.bottomBarURL.SetText("")
	l.hovered = false
}

func (l *hyperlink) MouseMoved(*desktop.MouseEvent) {
}

func (l *hyperlink) TappedSecondary(*fyne.PointEvent) {
}

func (l *hyperlink) Tapped(*fyne.PointEvent) {
	// TODO: Pass this over a chan?
	l.mw.GrabLink(l.url, false)
}

func (mw *mainWindow) NewLink(l string, url string) *hyperlink {
	label := widget.NewLabelWithStyle(l, fyne.TextAlignLeading, fyne.TextStyle{Bold: false, Italic: false, Monospace: true})

	hbox := widget.NewHBox(hyperlinkIcon, label)

	out := new(hyperlink)
	out.Box = hbox
	out.url = url
	out.mw = mw
	return out
}

func (mw *mainWindow) link2url(l string) *url.URL {
	base, err := url.Parse(mw.currentURI)
	if err != nil {
		log.Fatal(err)
	}
	u, err := url.Parse(l)
	if err != nil {
		log.Fatal(err)
	}
	if u.Path == "" {
		u.Path = "/"
	}
	u = base.ResolveReference(u)
	if strings.HasPrefix(l, "/") {
		u.Host = base.Host
	}
	return u
}
