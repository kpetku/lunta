package gui

import (
	"fyne.io/fyne/widget"
)

type footer struct {
	bottomBarURL *widget.Label
	progressBar  *widget.ProgressBarInfinite
}

func (f *footer) hideProgressBar() {
	f.progressBar.Stop()
	f.progressBar.Hide()
}

func (f *footer) showProgressBar() {
	f.progressBar.Start()
	f.progressBar.Show()
}
